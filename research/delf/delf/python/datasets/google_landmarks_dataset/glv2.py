# Copyright 2021 The TensorFlow Authors All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Google Landmarks Dataset(GLD)."""

import os
import pickle
import tensorflow as tf

from delf.python.datasets import tuples_dataset, utils


def landmark_id2filename(img_name, prefix):
  """Creates a training image path out of its ID name for the Google
  Landmarks v2 dataset.

  Args:
    img_name: String, image name.
    prefix: Root directory where images are saved.

  Returns:
    filename: String, full image filename.
  """
  return os.path.join(prefix, img_name[0], img_name[1], img_name[2], img_name)


class _Glv2(tuples_dataset.TuplesDataset):
  """Extends tuples_dataset.TuplesDataset."""

  def __init__(self, mode, data_root, imsize=None, nnum=5, qsize=2000,
               poolsize=20000, loader=utils.default_loader):
    """Google Landmarks v2 (Glv2) dataset initialization.

    Args:
      mode: Either 'train' or 'val'.
      data_root: Path to the root directory of the dataset.
      imsize: Integer, defines the maximum size of longer image side.
      nnum: Integer, number of negative images per one query.
      qsize: Integer, number of query images.
      poolsize: Integer, size of the negative image pool, from where the
        hard-negative images are chosen.
      loader: Callable, a function to load an image given its path.
    """
    if not (mode == 'train' or mode == 'val'):
      raise (RuntimeError(
        "MODE should be either train or val, passed as a string."))

    # Setting up paths.
    ims_root = os.path.join(data_root, 'images', 'train')
    name = "glv2"

    # Loading db.
    db_fn = os.path.join(data_root, '{}.pkl'.format(name))

    with tf.io.gfile.GFile(db_fn, 'rb') as f:
      db = pickle.load(f)[mode]

    # Setting full path for images.
    self.images = [landmark_id2filename(db['cids'][i] + '.jpg', ims_root)
                   for i in range(len(db['cids']))]

    # Initializing tuples dataset.
    self.name = name
    self.mode = mode
    self.imsize = imsize
    self.clusters = db['cluster']
    self.qpool = db['qidxs']
    self.ppool = db['pidxs']

    # Size of training subset for an epoch.
    self.nnum = nnum
    self.qsize = min(qsize, len(self.qpool))
    self.poolsize = min(poolsize, len(self.images))
    self.qidxs = None
    self.pidxs = None
    self.nidxs = None

    self.loader = loader
    self.print_freq = 10
    self.n = 0

  def Glv2Info(self):
    """Metadata for the Glv2 dataset.

    Returns:
      info: Dataset metadata.
    """
    info = {'train': {'clusters': 0, 'pidxs': 0, 'qidxs': 0},
            'val': {'clusters': 0, 'pidxs': 0, 'qidxs': 0}}
    return info


def CreateDataset(mode, data_root, imsize=None, nnum=5, qsize=2000,
                  poolsize=20000, loader=utils.default_loader):
  '''Creates Google Landmarks v2 (Glv2) dataset.

  Args:
    mode: Either 'train' or 'val'.
    data_root: Path to the root directory of the dataset.
    imsize: Integer, defines the maximum size of longer image side.
    nnum: Integer, number of negative images per one query.
    qsize: Integer, number of query images.
    poolsize: Integer, size of the negative image pool, from where the
      hard-negative images are chosen.
    loader: Callable, a function to load an image given its path.

  Returns:
    glv2: Glv2 dataset instance.
  '''
  return _Glv2(mode, data_root, imsize, nnum, qsize, poolsize, loader)
