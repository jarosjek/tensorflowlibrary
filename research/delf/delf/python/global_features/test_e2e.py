# Lint as: python3
# Copyright 2021 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Testing script for Global Features model."""

import os

import numpy as np
import tensorflow as tf
import pickle
import time

from absl import flags
from absl import app

from delf.python.datasets.revisited_op import dataset
from delf.python.datasets.revisited_op import dataset as testdataset
from delf.python.training import global_features_utils
from delf.python.training.model import global_model

_TEST_DATASET_NAMES = ['roxford5k', 'rparis6k']
_POOL_NAMES = ['mac', 'spoc', 'gem']
_LOSS_NAMES = ['contrastive', 'triplet']
_MODEL_NAMES = sorted(name for name in tf.keras.applications.__dict__ if not
name.startswith("__") and callable(tf.keras.applications.__dict__[name]))

FLAGS = flags.FLAGS

flags.DEFINE_enum('arch', default='ResNet101', enum_values=_MODEL_NAMES,
                  help='Model architecture: ' +
                       ' | '.join(_MODEL_NAMES) +
                       ' (default: ResNet101).')
flags.DEFINE_enum('pool', default='gem', enum_values=_POOL_NAMES,
                  help='Pooling options: ' +
                       ' | '.join(_POOL_NAMES) +
                       ' (default: gem).')
flags.DEFINE_bool('whitening', False,
                  help='Whether to train model with learnable whitening ('
                       'linear layer) after the pooling.')
flags.DEFINE_string('checkpoint_path', default=None,
                    help='Path to the network to be evaluated.')
flags.DEFINE_string('data_root', default="data", help='Path to the data.')
flags.DEFINE_string('test_datasets', default='roxford5k,rparis6k',
                    help='Comma separated list of test datasets: ' +
                         ' | '.join(_TEST_DATASET_NAMES) +
                         ' (default: roxford5k,rparis6k).')
flags.DEFINE_integer('image_size', default=1024,
                     help='Maximum size of longer image side used for '
                          'training (default: 1024).')
flags.DEFINE_string('multiscale', default='[1, 2**(1/2), 1/2**(1/2)]',
                    help="Use multiscale vectors for testing, " +
                         " examples: '[1]' | '[1, 1/2**(1/2), 1/2]' | '[1, "
                         "2**(1/2), 1/2**(1/2)]' (default: '[1]').")
flags.DEFINE_string('gpu_ids', default='0',
                    help='GPU id used for training (default: 0).')


def main(argv):
  global FLAGS

  # Manually check if there are unknown test datasets and if the dataset
  # ground truth files are downloaded.
  for dataset in FLAGS.test_datasets.split(','):
    if dataset not in _TEST_DATASET_NAMES:
      raise ValueError('Unsupported or unknown test dataset: {}.'.format(
        dataset))

  data_dir = os.path.join(FLAGS.data_root, "gnd_{}.pkl".format(dataset))
  if not os.path.isfile(data_dir):
    raise ValueError(
      '{} ground truth file at {} not found. Please download it according to '
      'the DELG instructions.'.format(dataset, FLAGS.data_root))

  # Set cuda visible device.
  os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_ids

  # Loading the network from the path.
  print(">> Loading network:\n>>>> '{}'".format(FLAGS.arch))

  net = global_model.GlobalFeatureNet(FLAGS.arch, FLAGS.pool,
                                      FLAGS.whitening, pretrained=False)

  net.build(input_shape=(None, None, None, 3))
  net.load_weights(FLAGS.checkpoint_path)
  print(">>>> Loaded checkpoint: ")
  print(net.meta_repr())

  save_directory = os.path.dirname(FLAGS.checkpoint_path)
  print("Metrics and calculated descriptors will be saved to: {}".format(
    save_directory))

  print(">> Testing step:")
  print('>> Evaluating network on test datasets...')

  # Evaluate on test datasets.
  datasets = FLAGS.test_datasets.split(',')
  for dataset in datasets:
    start = time.time()

    # Prepare config structure for the test dataset.
    cfg = testdataset.configdataset(dataset, os.path.join(
      FLAGS.data_root))
    images = [cfg['im_fname'](cfg, i) for i in range(cfg['n'])]
    qimages = [cfg['qim_fname'](cfg, i) for i in range(cfg['nq'])]
    bounding_boxes = [tuple(cfg['gnd'][i]['bbx']) for i in range(cfg['nq'])]

    # Extract database and query vectors.
    print(
      '>> {}: Extracting database images...'.format(dataset))
    vecs = global_model.extract_global_descriptors_from_list(
      net, images, FLAGS.image_size, ms=FLAGS.multiscale)
    print(
      '>> {}: Extracting query images...'.format(dataset))
    qvecs = global_model.extract_global_descriptors_from_list(
      net, qimages, FLAGS.image_size, bounding_boxes, ms=FLAGS.multiscale)

    global_features_utils.debug_and_log('>> {}: Evaluating...'.format(dataset))

    # Convert the obtained descriptors to numpy.
    vecs = vecs.numpy()
    qvecs = qvecs.numpy()

    # Save extracted vectors to a file.
    with tf.io.gfile.GFile(
            os.path.join(save_directory, '{}_descriptors.pkl'.format(dataset)),
            'wb') as desc_file:
      pickle.dump({"vecs" : vecs, "qvecs" : qvecs}, desc_file)

    # Search, rank and print test set metrics.
    scores = np.dot(vecs.T, qvecs)
    ranks = np.transpose(np.argsort(-scores, axis=0))
    easy_metrics, medium_metrics, hard_metrics = \
      global_features_utils.compute_metrics_and_print(dataset, ranks,
                                                               cfg['gnd'],
                                                               log=False)
    # Write metrics to file.
    mean_average_precision_dict = {
      'medium': medium_metrics[0],
      'hard': hard_metrics[0]
    }
    mean_precisions_dict = {'medium': medium_metrics[1],
                            'hard': hard_metrics[1]}
    mean_recalls_dict = {'medium': medium_metrics[2], 'hard': hard_metrics[2]}

    output_path = os.path.join(save_directory, 'metrics.txt')
    dataset.SaveMetricsFile(mean_average_precision_dict, mean_precisions_dict,
                          mean_recalls_dict, [1, 5, 10], output_path)

    print(
      '>> {}: Elapsed time: {}'.format(dataset, global_features_utils.htime(
        time.time() - start)))



if __name__ == '__main__':
  app.run(main)
