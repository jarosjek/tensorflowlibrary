## Global features: CNN Image Retrieval in Tensorflow

[![Paper](http://img.shields.io/badge/paper-arXiv.2001.05027-B3181B.svg)](https://arxiv.org/abs/1711.02512)
[![Paper](http://img.shields.io/badge/paper-arXiv.2001.05027-B3181B.svg)](http://arxiv.org/abs/1604.02426)

This Python toolbox implements the training and testing of the approach described in the papers:

**Fine-tuning CNN Image Retrieval with No Human Annotation**,  
Radenović F., Tolias G., Chum O., 
TPAMI 2018 [[arXiv](https://arxiv.org/abs/1711.02512)]

**CNN Image Retrieval Learns from BoW: Unsupervised Fine-Tuning with Hard Examples**,  
Radenović F., Tolias G., Chum O., 
ECCV 2016 [[arXiv](http://arxiv.org/abs/1604.02426)]

<img src="http://cmp.felk.cvut.cz/cnnimageretrieval/img/cnnimageretrieval_network_medium.png" width=\textwidth/>

**The code implements:**
1. Model definition

    In ```DELF_ROOT/delf/python/training/model/global_model.py```

    Network architecture initialization based on the input parameters. 
    Possibility of using pretrained models with several most common architectures (```DenseNet121 | DenseNet169 | 
    DenseNet201 | InceptionResNetV2 | InceptionV3 | MobileNet | MobileNetV2 | NASNetLarge | NASNetMobile | ResNet101 | 
    ResNet101V2 | ResNet152 | ResNet152V2 | ResNet50 | ResNet50V2 | VGG16 | VGG19 | Xception```). Defines the type of pooling and the presence and type of initialization of the last fully-connected layers (so-called “whitening”).
1. Training (fine-tuning) CNN for image retrieval

    In ```DELF_ROOT/delf/python/training/global_features/train.py```
    
    Allows to train the image retrieval model with the given parameters of the architecture and parameters required 
    during the training: learning rate, optimizer, momentum, weight decay, loss etc. Script allows to simultaneously 
    evaluate the training on the validation and test sets.
1. Supporting modules for  image loading, dataset structures, network layer definitions, exporting the global model 
in a way similar to delf, utilities (such as the checkpoint saving etc.)

    In ```DELF_ROOT/delf/python/global_features/```
1. Testing CNN image retrieval on Oxford and Paris datasets

    In ```DELF_ROOT/delf/python/global_features/extract_features.py```


### Install DELF library

To be able to use this code, please follow
[these instructions](../../../INSTALL_INSTRUCTIONS.md) to properly install the
DELF library.

### Usage

<details>
  <summary><b>Training</b></summary><br/>
  
  Navigate (```cd```) to the folder ```[DELF_ROOT/delf/python/training/global_features]```.
  Example training script is located in ```DELF_ROOT/delf/python/training/global_features/train.py```.
  ```
  python3 train.py [--arch ARCH] [--batch_size N] [--data_root PATH]
          [--debug] [--directory PATH] [--epochs N] [--gpu_id ID] 
          [--image_size SIZE] [--launch_tensorboard] [--loss LOSS] 
          [--loss_margin LM] [--lr LR] [--momentum M] [--neg_num N]
          [--pool POOL] [--pool_size N] [--pretrained] [--query_size N]
          [--test_datasets DATASET] [--test_freq N] [--test_whiten] 
          [--training_dataset DATASET] [--update_every N] [--val] [--val_eccv2020] 
          [--weight_decay N] [--whitening] [--workers N]
  ```

  For detailed explanation of the options run:
  ```
  python3 train.py -helpfull
  ```

  **Note**: Data and networks used for training and testing are automatically downloaded when using the example training
   script (```DELF_ROOT/delf/python/training/global_features/train.py```).

</details>

<details>
<summary><b>Training logic flow</b></summary><br/>

**Initialization phase**

1. Checking if required datasets are downloaded and automatically download them (both test and train/val) if they are 
not present in the data folder.
1. Setting up the logging and creating a logging/checkpoint directory.
1. Initialize model according to the user-provided parameters (architecture/pooling/whitening/pretrained).
1. Defining loss (contrastive/triplet) according to the user parameters.
1. Defining optimizer (Adam/SGD with learning rate/weight decay/momentum) according to the user parameters.
1. Initializing CheckpointManager and resuming from the latest checkpoint if the resume flag is set.
1. Launching Tensorboard if the flag is set.
1. Initializing training (and validation, if required) datasets.
1. Freezing BatchNorm weights update, since we we do training for one image at a time so the statistics would not be per batch, hence we choose freezing (i.e., using pretrained imagenet statistics).
1. Evaluating the network performance before training (on the test datasets); also used if we want to initialize the whitening layer (this is specified by the `test_whiten` flag).

**Training phase**

The main training loop (for the required number of epochs):
1. Finding the hard negative pairs in the dataset (using the forward pass through the model)
1. Creating the training dataset from generator (it changes every epoch). Each element in the dataset consists of 1xPositive image, 1xQuery image, NxHard negative images (N is specified by the `nnum` flag), an array specifying the Positive (-1), Query (0), Negative (1) images.
1. Performing one train step and calculate the final epoch loss.
1. If validation is required, find hard negatives in the validation set, which has the same structure as the training set. Perform one validation step and calculate the loss.
1. Evaluating on the test datasets every `test_freq` epochs.
1. Save checkpoint (optimizer and the model weights).

</details>

<details>
  <summary><b>Exporting global model</b></summary><br/>
  
  Navigate (```cd```) to the folder ```[DELF_ROOT/delf/python/global_features/utils]```.
  First, you will need to export a global model in a format specified in ```DELF_ROOT/delf/python/global_features/utils/export_global_model.py```.
  The script exports global feature model into the TF SavedModel format.
  ```
  python3 export_global_model.py [--ckpt_path N] 
          [--export_path N] [--arch N] [--pool N] 
          [--whitening N]
  ```
</details>

<details>
  <summary><b>Query feature extraction</b></summary><br/>
  
  Navigate (```cd```) to the folder ```[DELF_ROOT/delf/python/global_features]```.
  Query feature extraction is done in the `extract_features` script, when setting
`image_set=query`. We present here commands for extraction on `roxford5k`. Query feature extraction can be run as follows:
```
python3 extract_features.py \
  --config_path global_config.pbtxt \
  --dataset_file_path ~/data/gnd_roxford5k.mat \
  --images_dir ~/data/oxford5k_images \
  --image_set query \
  --output_features_dir ~/data/oxford5k_features/query
```
</details>


<details>
  <summary><b>Index feature extraction</b></summary><br/>
  
  Navigate (```cd```) to the folder ```[DELF_ROOT/delf/python/global_features]```.
  Run index feature extraction as follows:
  ```
python3 extract_features.py \
  --delf_config_path global_config.pbtxt \
  --dataset_file_path ~/data/gnd_roxford5k.mat \
  --images_dir ~/data/oxford5k_images \
  --image_set index \
  --output_features_dir ~/data/oxford5k_features/index
```
</details>

<details>
  <summary><b>Perform retrieval</b></summary><br/>
  
  Navigate (```cd```) to the folder ```[DELF_ROOT/delf/python/global_features]```.
  To run retrieval on `roxford5k`, the following command can be used:
```
python3 perform_retrieval.py \
  --dataset_file_path ~/data/gnd_roxford5k.mat \
  --query_features_dir ~/data/oxford5k_features/query \
  --index_features_dir ~/data/oxford5k_features/index \
  --output_dir ~/results/oxford5k
```
A file with named `metrics.txt` will be written to the path given in
`output_dir`, with retrieval metrics for an experiment where geometric
verification is not used. The contents should look approximately like:
```
hard
  mAP=...
  mP@k[ 1  5 10] [... ... ...]
  mR@k[ 1  5 10] [... ... ...]
medium
  mAP=...
  mP@k[ 1  5 10] [... ... ...]
  mR@k[ 1  5 10] [... ... ...]
```
</details>