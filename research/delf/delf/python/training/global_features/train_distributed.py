# Lint as: python3
# Copyright 2021 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Training script for Global Features model."""

import math
import os
import time

import numpy as np
import tensorflow as tf
import tensorflow_addons as tfa

from absl import flags
from absl import app
from absl import logging

from delf.python.datasets import tuples_dataset
from delf.python.datasets.revisited_op import dataset as testdataset
from delf.python.training.losses import ranking_losses
from delf.python.training import global_features_utils
from delf.python.training.model import global_model
from delf.python.datasets.sfm120k import sfm120k
from delf.python.datasets import utils

_TRAINING_DATASET_NAMES = ['retrieval-SfM-120k']
_TEST_DATASET_NAMES = ['roxford5k', 'rparis6k']

_MODEL_NAMES = sorted(name for name in tf.keras.applications.__dict__ if not
name.startswith("__") and callable(tf.keras.applications.__dict__[name]))

_POOL_NAMES = ['mac', 'spoc', 'gem']
_LOSS_NAMES = ['contrastive', 'triplet']
_OPTIMIZER_NAMES = ['sgd', 'adam']

FLAGS = flags.FLAGS

flags.DEFINE_boolean('debug', default=False, help='Debug mode.')

# Export directory, training and val datasets, test datasets.
flags.DEFINE_string('data_root', default="data", help='Path to the data.')
flags.DEFINE_string('directory', default="data",
                    help='Destination where trained network should be saved.')
flags.DEFINE_enum('training_dataset', default='retrieval-SfM-120k',
                  enum_values=_TRAINING_DATASET_NAMES,
                  help='Training dataset: ' +
                       ' | '.join(_TRAINING_DATASET_NAMES) +
                       ' (default: retrieval-SfM-120k).')
flags.DEFINE_bool('val', default=True, help='Whether to run validation.')
flags.DEFINE_bool('val_eccv2020', default=False,
                  help='New validation dataset used with ECCV 2020 paper.')
flags.DEFINE_string('test_datasets', default='roxford5k,rparis6k',
                    help='Comma separated list of test datasets: ' +
                         ' | '.join(_TEST_DATASET_NAMES) +
                         ' (default: roxford5k,rparis6k).')
flags.DEFINE_integer('test_freq', default=5,
                     help='Run test evaluation every N epochs (default: 1).')

# Network architecture and initialization options.
flags.DEFINE_enum('arch', default='ResNet101', enum_values=_MODEL_NAMES,
                  help='Model architecture: ' +
                       ' | '.join(_MODEL_NAMES) +
                       ' (default: ResNet101).')
flags.DEFINE_enum('pool', default='gem', enum_values=_POOL_NAMES,
                  help='Pooling options: ' +
                       ' | '.join(_POOL_NAMES) +
                       ' (default: gem).')
flags.DEFINE_bool('whitening', False,
                  help='Whether to train model with learnable whitening ('
                       'linear layer) after the pooling.')
flags.DEFINE_bool('pretrained', True,
                  help='Whether to initialize model with random weights ('
                       'default: pretrained on imagenet).')
flags.DEFINE_enum('loss', default='contrastive', enum_values=_LOSS_NAMES,
                  help='Training loss options: ' +
                       ' | '.join(_LOSS_NAMES) +
                       ' (default: contrastive).')
flags.DEFINE_float('loss_margin', default=0.7,
                   help='Loss margin: (default: 0.7).')

# train/val options specific for image retrieval learning.
flags.DEFINE_integer('image_size', default=1024,
                     help='Maximum size of longer image side used for '
                          'training (default: 1024).')
flags.DEFINE_integer('neg_num', default=5,
                     help='Number of negative image per train/val tuple ('
                          'default: 5).')
flags.DEFINE_integer('query_size', default=2000,
                     help='Number of queries randomly drawn per one train '
                          'epoch (default: 2000).')
flags.DEFINE_integer('pool_size', default=20000,
                     help='Size of the pool for hard negative mining ('
                          'default: 20000).')

# Standard train/val options.
flags.DEFINE_string('gpu_ids', default='0',
                    help='GPU id used for training (default: 0).')
flags.DEFINE_integer('workers', default=8,
                     help='Number of data loading workers (default: 8).')
flags.DEFINE_integer('epochs', default=100,
                     help='Number of total epochs to run (default: 100).')
flags.DEFINE_integer('batch_size', default=6,
                     help='Number of (q,p,n1,...,nN) tuples in a mini-batch ('
                          'default: 5).')
flags.DEFINE_integer('update_every', default=1,
                     help='Update model weights every N batches, used to '
                          'handle relatively large batches, ' +
                          'batch_size effectively becomes update_every x '
                          'batch_size (default: 1).')
flags.DEFINE_enum('optimizer', default='adam', enum_values=_OPTIMIZER_NAMES,
                  help='Optimizer options: ' +
                       ' | '.join(_OPTIMIZER_NAMES) +
                       ' (default: adam).')
flags.DEFINE_float('lr', default=1e-6,
                   help='Initial learning rate (default: 1e-6).')
flags.DEFINE_float('momentum', default=0.9, help='Momentum.')
flags.DEFINE_float('weight_decay', default=1e-6,
                   help='Weight decay (default: 1e-6).')
flags.DEFINE_integer('print_freq', default=10,
                     help='Print frequency (default: 10).')
flags.DEFINE_bool('resume', default=False,
                  help='Whether to start from the latest checkpoint in the '
                       'logdir.')
flags.DEFINE_bool('launch_tensorboard', False,
                  help='Whether to launch tensorboard.')
flags.DEFINE_bool('TPU', False,
                  help='Whether to use TPUs instead of GPUs.')

min_loss = float('inf')


def main(argv):
  global FLAGS, min_loss

  tf.debugging.set_log_device_placement(FLAGS.debug)
  options = tf.data.Options()
  options.experimental_distribute.auto_shard_policy = tf.data.experimental.AutoShardPolicy.DATA

  # Manually check if there are unknown test datasets and if the dataset
  # ground truth files are downloaded.
  for dataset in FLAGS.test_datasets.split(','):
    if dataset not in _TEST_DATASET_NAMES:
      raise ValueError('Unsupported or unknown test dataset: {}.'.format(
        dataset))

    data_dir = os.path.join(FLAGS.data_root, "gnd_{}.pkl".format(dataset))
    if not os.path.isfile(data_dir):
      raise ValueError(
        '{} ground truth file at {} not found. Please download it according to '
        'the DELG instructions.'.format(dataset, FLAGS.data_root))

  # Check if train dataset is downloaded and dowload it if not found.
  sfm120k.download_train(FLAGS.data_root)

  # Create export dir if it doesnt exist.
  directory = "{}".format(FLAGS.training_dataset)
  directory += "_{}".format(FLAGS.arch)
  directory += "_{}".format(FLAGS.pool)
  if FLAGS.whitening:
    directory += "_whiten"
  if not FLAGS.pretrained:
    directory += "_notpretrained"
  directory += "_{}_m{:.2f}".format(FLAGS.loss, FLAGS.loss_margin)
  directory += "_{}_lr{:.1e}_wd{:.1e}".format(FLAGS.optimizer, FLAGS.lr,
                                              FLAGS.weight_decay)
  directory += "_nnum{}_qsize{}_psize{}".format(FLAGS.neg_num,
                                                FLAGS.query_size,
                                                FLAGS.pool_size)
  directory += "_bsize{}_uevery{}_imsize{}".format(FLAGS.batch_size,
                                                   FLAGS.update_every,
                                                   FLAGS.image_size)

  FLAGS.directory = os.path.join(FLAGS.directory, directory)

  global_features_utils.debug_and_log(
    ">> Creating directory if does not exist:\n>> '{"
    "}'".format(FLAGS.directory))
  if not os.path.exists(FLAGS.directory):
    os.makedirs(FLAGS.directory)

  logdir = FLAGS.directory
  logging.get_absl_handler().use_absl_log_file('absl_logging', logdir)

  # Set cuda visible device.
  os.environ['CUDA_VISIBLE_DEVICES'] = FLAGS.gpu_ids
  global_features_utils.debug_and_log(">> Num GPUs Available: {}".format(
    len(tf.config.experimental.list_physical_devices('GPU'))), FLAGS.debug)

  # Set random seeds.
  tf.random.set_seed(0)
  np.random.seed(0)

  # Initialize model.
  if FLAGS.pretrained:
    global_features_utils.debug_and_log(
      ">> Using pre-trained model '{}'".format(FLAGS.arch))
  else:
    global_features_utils.debug_and_log(
      ">> Using model from scratch (random weights) '{}'.".format(
        FLAGS.arch))

  model_params = {'architecture': FLAGS.arch, 'pooling': FLAGS.pool,
                  'whitening': FLAGS.whitening,
                  'pretrained': FLAGS.pretrained}

  # Create the strategy.
  # Each GPU keeps its own loacl copy of every variable. Devices are kept in
  # sync by applying identical updates.
  if FLAGS.TPU:
    strategy = tf.distribute.experimental.TPUStrategy()
  else:
    strategy = tf.distribute.MirroredStrategy()

  global_features_utils.debug_and_log('Number of devices: {}'.format(
    strategy.num_replicas_in_sync), debug=True, log=True)

  # We do everything in distributed scope.
  with strategy.scope():
    # Network initialization.
    model = global_model.GlobalFeatureNet(**model_params)
    global_features_utils.debug_and_log(">> Network initialized.")

    # Loss initialization.
    global_features_utils.debug_and_log(">> Loss: {}.".format(FLAGS.loss))
    # Define loss function (criterion) and optimizer.
    if FLAGS.loss == 'contrastive':
      criterion = ranking_losses.ContrastiveLoss(margin=FLAGS.loss_margin)
    elif FLAGS.loss == 'triplet':
      criterion = ranking_losses.TripletLoss(margin=FLAGS.loss_margin)
    else:
      raise (RuntimeError("Loss {} not available!".format(FLAGS.loss)))

    start_epoch = 1
    initial_lr = FLAGS.lr
    exp_decay = math.exp(-0.01)
    decay_steps = FLAGS.query_size / FLAGS.batch_size

    # Define learning rate decay schedule.
    lr_scheduler = tf.keras.optimizers.schedules.ExponentialDecay(
      initial_learning_rate=initial_lr,
      decay_steps=decay_steps,
      decay_rate=exp_decay)

    # Define optimizer.
    if FLAGS.optimizer == 'sgd':
      opt = tfa.optimizers.extend_with_decoupled_weight_decay(
        tf.keras.optimizers.SGD)
      optimizer = opt(weight_decay=FLAGS.weight_decay,
                      learning_rate=lr_scheduler, momentum=FLAGS.momentum)
    elif FLAGS.optimizer == 'adam':
      opt = tfa.optimizers.extend_with_decoupled_weight_decay(
        tf.keras.optimizers.Adam)
      optimizer = opt(weight_decay=FLAGS.weight_decay,
                      learning_rate=lr_scheduler)

    # Logging and Tensorboard.
    writer = tf.summary.create_file_writer(logdir)

    # Setup checkpoint directory.
    checkpoint = tf.train.Checkpoint(optimizer=optimizer, model=model)
    manager = tf.train.CheckpointManager(
      checkpoint,
      logdir,
      max_to_keep=10,
      keep_checkpoint_every_n_hours=3)
    if FLAGS.resume:
      # Restores the checkpoint, if existing.
      global_features_utils.debug_and_log(">> Continuing from a checkpoint.")
      checkpoint.restore(manager.latest_checkpoint)

    global_features_utils.debug_and_log(
      ">> Training the {} network".format(directory))
    global_features_utils.debug_and_log(">> GPU ids: {}".format(FLAGS.gpu_ids))

    tf.summary.experimental.set_step(1)

    if FLAGS.launch_tensorboard:
      tensorboard = tf.keras.callbacks.TensorBoard(logdir)
      tensorboard.set_model(model=model)
      global_features_utils.launch_tensorboard(log_dir=logdir)

    # Log flags used.
    global_features_utils.debug_and_log('>> Running training script with:')
    global_features_utils.debug_and_log('>> logdir = {}'.format(logdir))

    if FLAGS.training_dataset.startswith("retrieval-SfM-120k"):
      train_dataset = sfm120k.CreateDataset(
        data_root=FLAGS.data_root,
        mode='train',
        imsize=FLAGS.image_size,
        nnum=FLAGS.neg_num,
        qsize=FLAGS.query_size,
        poolsize=FLAGS.pool_size,
        loader=utils.default_loader_resize
      )
      if FLAGS.val:
        val_dataset = sfm120k.CreateDataset(
          data_root=FLAGS.data_root,
          mode='val',
          imsize=FLAGS.image_size,
          nnum=FLAGS.neg_num,
          qsize=float('Inf'),
          poolsize=float('Inf'),
          eccv2020=FLAGS.val_eccv2020,
          loader=utils.default_loader_resize
        )
    else:
      train_dataset = tuples_dataset.TuplesDataset(
        name=FLAGS.training_dataset,
        data_root=FLAGS.data_root,
        mode='train',
        imsize=FLAGS.image_size,
        nnum=FLAGS.neg_num,
        qsize=FLAGS.query_size,
        poolsize=FLAGS.pool_size
      )
      if FLAGS.val:
        val_dataset = tuples_dataset.TuplesDataset(
          name=FLAGS.training_dataset,
          data_root=FLAGS.data_root,
          mode='val',
          imsize=FLAGS.image_size,
          nnum=FLAGS.neg_num,
          qsize=float('Inf'),
          poolsize=float('Inf')
        )

    # Freeze running mean and std:
    # We do training one image at a time, so the statistics would not be per
    # batch, hence we choose freezing (i.e., using imagenet statistics).
    for layer in model.feature_extractor.layers:
      if isinstance(layer, tf.keras.layers.BatchNormalization):
        layer.trainable = False

    with writer.as_default():
      for epoch in range(start_epoch, FLAGS.epochs + 1):

        # Set manual seeds per epoch.
        np.random.seed(epoch)
        tf.random.set_seed(epoch)

        # Find hard-negatives.
        # While hard-positive examples are fixed during the whole training
        # process and are randomly chosen from every epoch; hard-negatives
        # depend on the current CNN parameters and are re-mined once per epoch.
        avg_neg_distance = train_dataset.create_epoch_tuples(model)

        def train_gen():
          for inst in train_dataset:
            images = tf.convert_to_tensor(inst[0])
            labels = tf.convert_to_tensor(inst[1])
            yield (images, labels)

        train_loader = tf.data.Dataset.from_generator(
          train_gen,
          output_signature=(
            tf.TensorSpec(shape=(
              FLAGS.neg_num + 2,
              FLAGS.image_size,
              FLAGS.image_size, 3),
              dtype=tf.int32),
            tf.TensorSpec(shape=(
                    FLAGS.neg_num + 2,), dtype=tf.int32))).with_options(
          options).batch(FLAGS.batch_size)
        train_dist_dataset = strategy.experimental_distribute_dataset(
          train_loader)

        loss = train_val(strategy, loader=train_dist_dataset, model=model,
                         criterion=criterion, optimizer=optimizer,
                         epoch=epoch)
        # Write a scalar summary.
        tf.summary.scalar("train_epoch_loss", loss, step=epoch)
        # Forces summary writer to send any buffered data to storage.
        writer.flush()

        # Evaluate on validation set.
        if FLAGS.val and (epoch % FLAGS.test_freq == 0 or epoch == 1):
          avg_neg_distance = val_dataset.create_epoch_tuples(model)

          def val_gen():
            for inst in val_dataset:
              images = tf.convert_to_tensor(inst[0])
              labels = tf.convert_to_tensor(inst[1])
              yield (images, labels)

          val_loader = tf.data.Dataset.from_generator(
            val_gen,
            output_signature=(tf.TensorSpec(
              shape=(FLAGS.neg_num + 2,
                     FLAGS.image_size,
                     FLAGS.image_size,
                     3), dtype=tf.float32),
                              tf.TensorSpec(shape=(
                                      FLAGS.neg_num + 2),
                                dtype=tf.int32))).with_options(
            options).batch(FLAGS.batch_size)
          validation_dist_dataset = strategy.experimental_distribute_dataset(
            val_loader)

          loss = train_val(strategy, loader=validation_dist_dataset,
                           model=model,
                           criterion=criterion, optimizer=None,
                           epoch=epoch, train=False)
          tf.summary.scalar("val_epoch_loss", loss, step=epoch)
          writer.flush()

        # Evaluate on test datasets every test_freq epochs.
        if epoch == 1 or epoch % FLAGS.test_freq == 0:
          test(FLAGS.test_datasets, model, writer=writer, epoch=epoch)

        # Saving checkpoints and model weights.
        try:
          save_path = manager.save(checkpoint_number=epoch)
          global_features_utils.debug_and_log(
            'Saved ({}) at {}'.format(epoch, save_path))

          filename = os.path.join(logdir,
                                  'checkpoint_epoch_{}_loss_{}.h5'.format(
                                    epoch, min_loss))
          model.save_weights(filename, save_format="h5")
          global_features_utils.debug_and_log(
            'Saved weights ({}) at {}'.format(epoch, filename))
        except Exception as ex:
          global_features_utils.debug_and_log(
            "Could not save checkpoint: {}".format(ex))


def grad(criterion, model, input, target):
  """Records gradients and loss through the network..

  Args:
    criterion: Loss function.
    model: Network for the gradient computation.
    input: Tuple of query, positive and negative images.
    target: List of indexes to specify queries (-1), positives(1), negatives(0).

  Returns:
    loss: Loss for the training step.
    gradients: Computed gradients for the network trainable variables.
  """
  # Record gradients and loss through the network.
  with tf.GradientTape() as tape:
    output = tf.Variable(
      tf.zeros(shape=(0, model.meta['outputdim']), dtype=tf.float32))
    for img in input:
      # Compute descriptor vector for each image.
      o = model(tf.expand_dims(img, axis=0), training=True)
      output = tf.concat([output, o], 0)

    queries = tf.boolean_mask(output, target == -1, axis=0)
    positives = tf.boolean_mask(output, target == 1, axis=0)
    negatives = tf.boolean_mask(output, target == 0, axis=0)
    negatives = tf.reshape(negatives, [tf.shape(queries)[0], FLAGS.neg_num,
                                       model.meta['outputdim']])
    # Loss calculation.
    loss = criterion(queries, positives, negatives)

  return loss, tape.gradient(loss, model.trainable_variables)


def train_step(accum_grads, criterion, model, input, target, losses, optimizer,
               batch_num, all_batch_num, tvs):
  # Train on one batch.
  for input_q, target_q in zip(input, target):
    loss_value, grads = grad(criterion, model, input_q, target_q)
    losses.update_state(loss_value)
    # Adds to each element from the list you initialized earlier
    # with zeros its gradient (works because accum_vars and gvs
    # are in the same order).
    accum_grads = [accum_grads[i].assign_add(gv) for i, gv in
                   enumerate(grads)]

  if (batch_num + 1) % FLAGS.update_every == 0 or (
          batch_num + 1) == all_batch_num:
    # Do one step for multiple batches. Accumulated gradients are
    # used.
    optimizer.apply_gradients(
      zip(accum_grads, model.trainable_variables))
    accum_grads = [
      tf.Variable(tf.zeros_like(tv.read_value()), trainable=False)
      for tv in tvs]
  return


def val_step(model, criterion, target, losses):
  output = tf.Variable(
    tf.zeros(shape=(0, model.meta['outputdim']), dtype=tf.float32))
  nq = 0
  for input_q in input:
    nq += 1
    for imi in input_q:
      # Compute output vector for image imi of query q.
      o = tf.expand_dims(imi, axis=0)
      model_out = model(o, training=False)
      output = tf.concat([output, model_out], 0)

  # No need to reduce memory consumption (no backward pass):
  # Compute loss for the full batch.
  tmp_target = tf.concat(target, axis=0)
  queries = tf.boolean_mask(output, tmp_target == -1, axis=0)
  positives = tf.boolean_mask(output, tmp_target == 1, axis=0)
  negatives = tf.boolean_mask(output, tmp_target == 0, axis=0)
  negatives = tf.reshape(negatives, [tf.shape(queries)[0], FLAGS.neg_num,
                                     model.meta['outputdim']])
  loss = criterion(queries, positives, negatives)

  # Record loss.
  losses.update_state(loss / nq, nq)


#@tf.function
def train_val(strategy, loader, model, criterion, optimizer, epoch,
               train=True):
  """Executes either training or validation step based on `train` value.

  Args:
    loader: Training/validation dataset.
    model: Network to train/validate.
    criterion: Loss function.
    optimizer: Network optimizer.
    epoch: Integer, epoch number.
    train: Bool, specifies training or validation phase.

  Returns:
    average_epoch_loss: Average epoch loss.
  """

  # batch_time = global_features_utils.AverageMeter()
  # data_time = global_features_utils.AverageMeter()
  # losses = global_features_utils.AverageMeter()


  batch_time = tf.keras.metrics.Mean(name="batch_time")
  data_time = tf.keras.metrics.Mean(name="data_time")
  losses = tf.keras.metrics.Mean(name="losses")

  # Retrieve all trainable variables we defined in the graph.
  tvs = model.trainable_variables
  accum_grads = [tf.Variable(tf.zeros_like(tv.read_value()), trainable=False)
                 for tv in tvs]

  end = time.time()
  batch_num = 0
  all_batch_num = FLAGS.query_size // FLAGS.batch_size
  state = "Train" if train else "Val"
  global_features_utils.debug_and_log(">> {} step:".format(state))

  # For every batch in the dataset; Stops when all batches in the dataset have
  # been processed.
  for batch in loader:
    input, target = batch
    data_time.update(time.time() - end)

    if train:
      # Train on one batch.
      print("training on one batch")
      strategy.run(train_step,
                   args=(accum_grads, criterion, model, input, target, losses,
                         optimizer,
                         batch_num, all_batch_num, tvs,))
    else:
      # Validate one batch.
      print("validating on one batch")
      strategy.run(val_step, args=(model, criterion, target, losses,))

    # Measure elapsed time.
    batch_time.update_state(time.time() - end)
    end = time.time()

    # Record immediate loss and elapsed time.
    if FLAGS.debug and ((batch_num + 1) % FLAGS.print_freq == 0 or
                        batch_num == 0 or (batch_num + 1) == all_batch_num):
      global_features_utils.debug_and_log(
        '>> {0}: [{1} epoch][{2}/{3} batch]\t Time val: {batch_time.val:.3f} '
        '(Batch Time avg: {batch_time.avg:.3f})\t Data {data_time.val:.3f} ('
        'Time avg: {data_time.avg:.3f})\t Immediate loss value: {loss.val:.4f} '
        '(Loss avg: {loss.avg:.4f})'.format(state, epoch, batch_num + 1,
                                            all_batch_num,
                                            batch_time=batch_time,
                                            data_time=data_time,
                                            loss=losses), debug=True, log=False)
    batch_num += 1

  return losses.result().numpy()


def test(datasets, net, epoch, writer=None):
  """Testing step.

  Evaluates the network on the provided test datasets by computing single-scale
  mAP for easy/medium/hard cases. If `writer` is specified, saves the mAP
  values in a tensorboard supported format.

  Args:
    datasets: List of dataset names for model testing (from
      `_TEST_DATASET_NAMES`).
    net: Network to evaluate.
    epoch: Integer, epoch number.
    writer: Tensorboard writer.
  """
  global_features_utils.debug_and_log(">> Testing step:")
  global_features_utils.debug_and_log(
    '>> Evaluating network on test datasets...')

  # For testing we use image size of max 1024.
  image_size = 1024

  # Evaluate on test datasets.
  datasets = datasets.split(',')
  for dataset in datasets:
    start = time.time()

    # Prepare config structure for the test dataset.
    cfg = testdataset.configdataset(dataset, os.path.join(
      FLAGS.data_root))
    images = [cfg['im_fname'](cfg, i) for i in range(cfg['n'])]
    qimages = [cfg['qim_fname'](cfg, i) for i in range(cfg['nq'])]
    bounding_boxes = [tuple(cfg['gnd'][i]['bbx']) for i in range(cfg['nq'])]

    # Extract database and query vectors.
    global_features_utils.debug_and_log(
      '>> {}: Extracting database images...'.format(dataset))
    vecs = global_model.extract_global_descriptors_from_list(net, images,
                                                             image_size)
    global_features_utils.debug_and_log(
      '>> {}: Extracting query images...'.format(dataset))
    qvecs = global_model.extract_global_descriptors_from_list(net, qimages,
                                                              image_size,
                                                              bounding_boxes)
    global_features_utils.debug_and_log('>> {}: Evaluating...'.format(dataset))

    # Convert the obtained descriptors to numpy.
    vecs = vecs.numpy()
    qvecs = qvecs.numpy()

    # Search, rank and print test set metrics.
    scores = np.dot(vecs.T, qvecs)
    ranks = np.transpose(np.argsort(-scores, axis=0))
    accuracy = global_features_utils.compute_metrics_and_print(dataset, ranks,
                                                               cfg['gnd'])
    # Save calculated metrics in a tensorboard format.
    if writer:
      tf.summary.scalar("test_accuracy_{}_E".format(dataset), accuracy[0][0],
                        step=epoch)
      tf.summary.scalar("test_accuracy_{}_M".format(dataset), accuracy[1][0],
                        step=epoch)
      tf.summary.scalar("test_accuracy_{}_H".format(dataset), accuracy[2][0],
                        step=epoch)
      writer.flush()

    global_features_utils.debug_and_log(
      '>> {}: elapsed time: {}'.format(dataset, global_features_utils.htime(
        time.time() - start)))


if __name__ == '__main__':
  app.run(main)
