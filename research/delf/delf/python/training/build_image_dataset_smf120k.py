# Copyright 2020 The TensorFlow Authors All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""
Example usage:
    python3 build_image_dataset_sfm120k \
    --num_shards=128 \
    --output_directory=/mnt/datagrid/personal/jarosjek/citorch_data/train/retrieval-SfM-120k/TFRecords2/ \
    --data_root=/mnt/datagrid/personal/jarosjek/citorch_data/train/retrieval-SfM-120k/
"""

import os
import pickle

from absl import app
from absl import flags

from delf.python.datasets.sfm120k import sfm120k
from delf.python.training.build_image_dataset import _write_tfrecord

FLAGS = flags.FLAGS

flags.DEFINE_string('data_root', '/tmp/', 'Sfm120k data directory.')
flags.DEFINE_boolean('val_eccv2020', False, 'New validation dataset used with ECCV 2020 paper.')

_TRAIN_SPLIT = 'train'
_VALIDATION_SPLIT = 'validation'


def _get_all_image_files_and_labels_sfm120k(name, mode, data_root):
    """Process input and get the image file paths, image ids and the labels.
  Args:
    name: dataset name.
    mode: 'train' or 'val'.
    data_root: root directory of smf120k.
  Returns:
    image_paths: the paths to all images in the image_dir.
    file_ids: the unique ids of images.
    labels: the landmark id of all images. When name='test', the returned labels
      will be an empty list.
  Raises:
    ValueError: if input name is not supported.
  """
    ims_root = os.path.join(data_root, 'ims')

    # Loading db.
    db_fn = os.path.join(data_root, '{}.pkl'.format(name))
    with tf.io.gfile.GFile(db_fn, 'rb') as f:
        db = pickle.load(f)[mode]

    # Setting full path for the images.
    image_paths = [sfm120k.id2filename(db['cids'][i], ims_root) for i in
                   range(len(db['cids']))]
    return image_paths, db['cids'], db['cluster']


def _build_train_tfrecord_dataset_sfm120k(name, mode, data_root):
    # Load images.
    image_paths, file_ids, labels = _get_all_image_files_and_labels_sfm120k(
        name, mode, data_root)

    if mode == "train":
        _write_tfrecord(_TRAIN_SPLIT, image_paths, file_ids, labels)
    elif mode == "val":
        _write_tfrecord(_VALIDATION_SPLIT, image_paths, file_ids, labels)


def main(unused_argv):
    if FLAGS.val_eccv2020:
        name = "retrieval-SfM-120k-val-eccv2020"
    else:
        name = "retrieval-SfM-120k"

    print("Writing training split.")
    _build_train_tfrecord_dataset_sfm120k(name, mode='train', data_root=FLAGS.data_root)
    print("Writing validation split.")
    _build_train_tfrecord_dataset_sfm120k(name, mode='val', data_root=FLAGS.data_root)


if __name__ == '__main__':
    app.run(main)
