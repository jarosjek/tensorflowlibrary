import os
import time

import tensorflow as tf
import numpy as np


from delf.python.pooling_layers import pooling as pooling_layers
from delf.python.datasets.revisited_op import dataset as testdataset
from delf.python.datasets import generic_dataset

import tensorflow.python.keras.layers as layers
from tensorflow.python.keras.applications import imagenet_utils
from tensorflow.python.keras import backend
from tensorflow.python.keras.engine import training
from tensorflow.python.keras.utils import data_utils
from tensorflow.python.keras.utils import layer_utils
from tensorflow.python.lib.io import file_io


WEIGHTS_PATH_NO_TOP = ('https://storage.googleapis.com/tensorflow/'
                       'keras-applications/vgg16/'
                       'vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5')

def VGG16_Max(
    include_top=True,
    weights='imagenet',
    input_tensor=None,
    input_shape=None,
    pooling=None,
    classes=1000,
    classifier_activation='softmax'):
  if not (weights in {'imagenet', None} or file_io.file_exists_v2(weights)):
    raise ValueError('The `weights` argument should be either '
                     '`None` (random initialization), `imagenet` '
                     '(pre-training on ImageNet), '
                     'or the path to the weights file to be loaded.')

  if weights == 'imagenet' and include_top and classes != 1000:
    raise ValueError('If using `weights` as `"imagenet"` with `include_top`'
                     ' as true, `classes` should be 1000')
  # Determine proper input shape
  input_shape = imagenet_utils.obtain_input_shape(
      input_shape,
      default_size=224,
      min_size=32,
      data_format=backend.image_data_format(),
      require_flatten=include_top,
      weights=weights)

  if input_tensor is None:
    img_input = layers.Input(shape=input_shape)
  else:
    if not backend.is_keras_tensor(input_tensor):
      img_input = layers.Input(tensor=input_tensor, shape=input_shape)
    else:
      img_input = input_tensor
  # Block 1
  x = layers.Conv2D(
      64, (3, 3), activation='relu', padding='same', name='block1_conv1')(
          img_input)
  x = layers.Conv2D(
      64, (3, 3), activation='relu', padding='same', name='block1_conv2')(x)
  #print("Max1 input: ", x)
  x = layers.MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool')(x)
  #print("Max1 out: ", x)

  # Block 2
  x = layers.Conv2D(
      128, (3, 3), activation='relu', padding='same', name='block2_conv1')(x)
  x = layers.Conv2D(
      128, (3, 3), activation='relu', padding='same', name='block2_conv2')(x)
  #print("Max2 input: ", x)
  x = layers.MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool')(x)
  #print("Max2 out: ", x)

  # Block 3
  x = layers.Conv2D(
      256, (3, 3), activation='relu', padding='same', name='block3_conv1')(x)
  x = layers.Conv2D(
      256, (3, 3), activation='relu', padding='same', name='block3_conv2')(x)
  x = layers.Conv2D(
      256, (3, 3), activation='relu', padding='same', name='block3_conv3')(x)

  x = layers.MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool')(x)
  #print("Max3 out: ", x)

  # Block 4
  x = layers.Conv2D(
      512, (3, 3), activation='relu', padding='same', name='block4_conv1')(x)
  x = layers.Conv2D(
      512, (3, 3), activation='relu', padding='same', name='block4_conv2')(x)
  x = layers.Conv2D(
      512, (3, 3), activation='relu', padding='same', name='block4_conv3')(x)
  x = layers.MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool')(x)
  #print("Max4 out: ", x)

  # Block 5
  x = layers.Conv2D(
      512, (3, 3), activation='relu', padding='same', name='block5_conv1')(x)
  x = layers.Conv2D(
      512, (3, 3), activation='relu', padding='same', name='block5_conv2')(x)
  x = layers.Conv2D(
      512, (3, 3), activation='relu', padding='same', name='block5_conv3')(x)
  x = layers.MaxPooling2D((2, 2), strides=(2, 2), name='block5_pool')(x)
  #print("Max5 out: ", x)


  # Ensure that the model takes into account
  # any potential predecessors of `input_tensor`.
  if input_tensor is not None:
    inputs = layer_utils.get_source_inputs(input_tensor)
  else:
    inputs = img_input
  # Create model.
  model = training.Model(inputs, x, name='vgg16')

  # Load weights.
  if weights == 'imagenet':
    weights_path = data_utils.get_file(
          'vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5',
          WEIGHTS_PATH_NO_TOP,
          cache_subdir='models',
          file_hash='6d6bbae143d832006294945121d1f1fc')
    model.load_weights(weights_path)
  elif weights is not None:
    model.load_weights(weights)

  return model




def VGG16_Gem(p=5.,
    include_top=True,
    weights='imagenet',
    input_tensor=None,
    input_shape=None,
    pooling=None,
    classes=1000,
    classifier_activation='softmax'):
  if not (weights in {'imagenet', None} or file_io.file_exists_v2(weights)):
    raise ValueError('The `weights` argument should be either '
                     '`None` (random initialization), `imagenet` '
                     '(pre-training on ImageNet), '
                     'or the path to the weights file to be loaded.')

  if weights == 'imagenet' and include_top and classes != 1000:
    raise ValueError('If using `weights` as `"imagenet"` with `include_top`'
                     ' as true, `classes` should be 1000')
  # Determine proper input shape
  input_shape = imagenet_utils.obtain_input_shape(
      input_shape,
      default_size=224,
      min_size=32,
      data_format=backend.image_data_format(),
      require_flatten=include_top,
      weights=weights)

  if input_tensor is None:
    img_input = layers.Input(shape=input_shape)
  else:
    if not backend.is_keras_tensor(input_tensor):
      img_input = layers.Input(tensor=input_tensor, shape=input_shape)
    else:
      img_input = input_tensor
  # Block 1
  x = layers.Conv2D(
      64, (3, 3), activation='relu', padding='same', name='block1_conv1')(
          img_input)
  x = layers.Conv2D(
      64, (3, 3), activation='relu', padding='same', name='block1_conv2')(x)
  x = pooling_layers.GeMPooling2D(p, (2, 2), strides=(2, 2))(x)

  # Block 2
  x = layers.Conv2D(
      128, (3, 3), activation='relu', padding='same', name='block2_conv1')(x)
  x = layers.Conv2D(
      128, (3, 3), activation='relu', padding='same', name='block2_conv2')(x)
  x = pooling_layers.GeMPooling2D(p, (2, 2), strides=(2, 2))(x)

  # Block 3
  x = layers.Conv2D(
      256, (3, 3), activation='relu', padding='same', name='block3_conv1')(x)
  x = layers.Conv2D(
      256, (3, 3), activation='relu', padding='same', name='block3_conv2')(x)
  x = layers.Conv2D(
      256, (3, 3), activation='relu', padding='same', name='block3_conv3')(x)
  x = pooling_layers.GeMPooling2D(p, (2, 2), strides=(2, 2))(x)

  # Block 4
  x = layers.Conv2D(
      512, (3, 3), activation='relu', padding='same', name='block4_conv1')(x)
  x = layers.Conv2D(
      512, (3, 3), activation='relu', padding='same', name='block4_conv2')(x)
  x = layers.Conv2D(
      512, (3, 3), activation='relu', padding='same', name='block4_conv3')(x)
  x = pooling_layers.GeMPooling2D(p, (2, 2), strides=(2, 2))(x)

  # Block 5
  x = layers.Conv2D(
      512, (3, 3), activation='relu', padding='same', name='block5_conv1')(x)
  x = layers.Conv2D(
      512, (3, 3), activation='relu', padding='same', name='block5_conv2')(x)
  x = layers.Conv2D(
      512, (3, 3), activation='relu', padding='same', name='block5_conv3')(x)
  x = pooling_layers.GeMPooling2D(p, (2, 2), strides=(2, 2))(x)


  # Ensure that the model takes into account
  # any potential predecessors of `input_tensor`.
  if input_tensor is not None:
    inputs = layer_utils.get_source_inputs(input_tensor)
  else:
    inputs = img_input
  # Create model.
  model = training.Model(inputs, x, name='vgg16')

  # Load weights.
  if weights == 'imagenet':
    weights_path = data_utils.get_file(
          'vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5',
          WEIGHTS_PATH_NO_TOP,
          cache_subdir='models',
          file_hash='6d6bbae143d832006294945121d1f1fc')
    model.load_weights(weights_path)
  elif weights is not None:
    model.load_weights(weights)

  return model



def gem_vs_mac_speed_compare():
  x = tf.random.uniform([500, 2, 2, 2048])
  # init all stuff.
  pooling_layers.spoc(x)


  print("Input shape: ", tf.shape(x))
  start_mac = time.time()
  mac_pooled_descriptors = pooling_layers.mac(x)
  print("MAC time: ", time.time() - start_mac)
  print("Output shape: ", mac_pooled_descriptors.shape)



  p = 3.
  start_gem = time.time()
  gem_pooled_descriptors = pooling_layers.gem(x, power=p)

  print("GeM (p = {}) time: {}".format(p, time.time() - start_gem))

  start_gem = time.time()
  p = 5.
  gem_pooled_descriptors = pooling_layers.gem(x, power=p)
  print("GeM (p = {}) time: {}".format(p, time.time() - start_gem))

  start_gem = time.time()
  p = 10.
  gem_pooled_descriptors = pooling_layers.gem(x, power=p)
  print("GeM (p = {}) time: {}".format(p, time.time() - start_gem))
  print("Output shape: ", gem_pooled_descriptors.shape)

  print("Compare the outputs")
  print("MAC: ", mac_pooled_descriptors[0])
  print("GeM: ", gem_pooled_descriptors[0])


def check_VGG_speed_with_and_without_GeM(pool_type='max', p=5.):

  architecture = "VGG16"
  dataset = 'roxford5k'
  image_size = 1024
  print_freq = 10
  data_root = '/home/cvut/data/'
  #'/home/cvut/data/'
  # #'/mnt/datagrid/personal/jarosjek/citorch_data'

  # if replace_Max:
  #   net = VGG16_Gem(p=p, include_top=False, weights="imagenet")
  # else:
  #   net = VGG16_Max(include_top=False, weights="imagenet")


  backbone = tf.keras.applications.VGG19(include_top=False, weights="imagenet")

  count_max_pool = 0
  net = tf.keras.Sequential()
  net.add(tf.keras.layers.InputLayer(input_shape=(None, None, 3)))
  for layer in backbone.layers:
    if 'max' not in pool_type and isinstance(layer,
                                            tf.keras.layers.MaxPooling2D):
      pool_size = layer.pool_size
      strides = layer.strides
      padding = layer.padding
      data_format = layer.data_format
      count_max_pool += 1
      if 'avg' in pool_type:
        net.add(tf.keras.layers.AveragePooling2D(pool_size, strides,
                                         padding, data_format))
        print("added avg")
      elif 'gem' in pool_type:
        net.add(pooling_layers.GeMPooling2D(p, pool_size, strides,
                                                          padding,
                                            data_format))
        print("added gem")
    elif isinstance(layer, tf.keras.layers.BatchNormalization):
      layer.trainable = False
      net.add(layer)
    else:
      net.add(layer)
  print("Replaced {} MaxPooling2D layers with other pooling.".format(
    count_max_pool))




  # Prepare config structure for the test dataset.
  cfg = testdataset.configdataset(dataset, os.path.join(
    data_root))
  images = [cfg['im_fname'](cfg, i) for i in range(cfg['n'])]

  # Extract database and query vectors.
  print('>> {}: Extracting database images...'.format(dataset))
  data = generic_dataset.ImagesFromList(root='', image_paths=images,
                                        imsize=image_size)

  def data_gen():
    return (inst for inst in data)

  loader = tf.data.Dataset.from_generator(data_gen, output_types=(tf.float32))
  loader = loader.batch(1)

  start_time = time.time()

  # Extracting vectors.
  for i, input in enumerate(loader):
    vec = net(input, training=False)
    #print("\nNetwork final output: ", vec)

    if i == 9:
      print("\nfor 10 imgs (1024) time: {}".format(
                                                      time.time()-start_time))

    if i == 99:
      print("\nfor 100 imgs (1024) time: {}".format(
                                                      time.time()-start_time))

    if i == 499:
      print("\nfor 500 imgs (1024) time: {}".format(
                                                      time.time()-start_time))
      print("Reached 500 images. Done.")
      break

    if (i + 1) % print_freq == 0 or (i + 1) == len(images):
      print('\r>>>> {}/{} done...'.format((i + 1), len(images)), end='')
  print('')


if __name__=='__main__':
  os.environ['CUDA_VISIBLE_DEVICES'] = '2'
  gem_vs_mac_speed_compare()
  #
  # print("IGNORE THIS ONE")
  # check_VGG_speed_with_and_without_GeM(pool_type='gem', p=1.)
  # print("IGNORE---------------------------------")
  #
  # print("Testing VGG speed with GeMPooling2D p=1.")
  # check_VGG_speed_with_and_without_GeM(pool_type='gem', p=1.)
  # print("OK---------------------------------")
  #
  # print("Testing VGG speed with GeMPooling2D p=3.")
  # check_VGG_speed_with_and_without_GeM(pool_type='gem', p=3.)
  # print("OK---------------------------------")
  #
  # print("Testing VGG speed with GeMPooling2D p=5.")
  # check_VGG_speed_with_and_without_GeM(pool_type='gem', p=5.)
  # print("OK---------------------------------")
  #
  # print("Testing VGG speed with MaxPooling2D.")
  # check_VGG_speed_with_and_without_GeM(pool_type='max')
  # print("OK---------------------------------")
  #
  # print("Testing VGG speed with AvgPooling2D.")
  # check_VGG_speed_with_and_without_GeM(pool_type='avg')
  # print("OK---------------------------------")
