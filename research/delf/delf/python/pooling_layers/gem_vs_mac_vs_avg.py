import os
import time

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

from delf.python.pooling_layers import pooling as pooling_layers
from delf.python.datasets.revisited_op import dataset as testdataset
from delf.python.datasets import generic_dataset

import tensorflow.python.keras.layers as layers
from delf.python.pooling_layers import pooling


def measure_matrix_diff(A, B):
  out = np.absolute(A - B)
  out = np.linalg.norm(out)
  out = out / A.size
  return out

def pool_test_2d(sparsity=0.5):
  # Define a test matrix.
  X = np.random.rand(1, 100, 100, 1)

  # Randomly set some elements to zero.
  indices = np.random.choice(X.size,
                             replace=False,
                             size=int(X.size * sparsity))
  X[np.unravel_index(indices, X.shape)] = 0

  # Convert X to tensor.
  X = tf.constant(X, dtype=tf.float32)

  # Define the avg, max, gem poolings with the same size.
  psize = 2

  max_pool = tf.keras.layers.MaxPooling2D(pool_size=(psize, psize),
                                          strides=(1, 1), padding='valid')
  avg_pool = tf.keras.layers.AveragePooling2D(pool_size=(psize, psize),
                                              strides=(1, 1),
                                              padding='valid')

  # We will measure the difference between Max and Avg first.
  max_out = max_pool(X).numpy()
  avg_out = avg_pool(X).numpy()
  madiff = measure_matrix_diff(max_out, avg_out)

  # For different values of `power` measure distance between Gem and Max,
  # Gem and Avg.

  powers = np.arange(1., 50, 0.5)
  gmdiff = []
  gadiff = []

  for power in powers:
    gem_pool = pooling.GeMPooling2D(power=power, pool_size=(psize, psize),
                                    strides=(1, 1), padding='valid')
    gem_out = gem_pool(X).numpy()
    gmdiff.append(measure_matrix_diff(gem_out, max_out))
    gadiff.append(measure_matrix_diff(gem_out, avg_out))

  # Visualization.
  plt.figure()
  plt.plot(powers, [madiff] * len(powers), linestyle='dashed', color='green', \
           label='Avg')
  plt.plot(powers, [0] * len(powers), linestyle='dashed', color='red',
           label='Max')
  plt.plot(powers, gmdiff, color='blue', label='Gem', linestyle='dashed') #,
  # marker='o')
  plt.xlabel('Gem power')
  plt.ylabel('Matrix difference')
  plt.xlim([0, max(powers)+1])
  plt.legend()
  plt.grid(linestyle = '--', linewidth = 0.5)
  plt.show()

def pool_test_1d(sparsity=0.5):
  # Define a test matrix.
  X = np.random.rand(1, 20, 20, 2048) * 255

  # Randomly set some elements to zero.
  indices = np.random.choice(X.size,
                             replace=False,
                             size=int(X.size * sparsity))
  X[np.unravel_index(indices, X.shape)] = 0

  # Convert X to tensor.
  X = tf.constant(X)

  # Define the avg, max, gem poolings with the same size.
  psize = 2

  # We will measure the difference between Max and Avg first.
  max_out = pooling.mac(X).numpy()
  avg_out = pooling.spoc(X).numpy()
  madiff = measure_matrix_diff(max_out, avg_out)

  # For different values of `power` measure distance between Gem and Max,
  # Gem and Avg.

  powers = np.arange(1., 15.25, 0.25)
  gmdiff = []
  gadiff = []

  for power in powers:
    gem_out = pooling.gem(X, power=power).numpy()
    gmdiff.append(measure_matrix_diff(gem_out, max_out))
    gadiff.append(measure_matrix_diff(gem_out, avg_out))

  # Visualization.
  plt.figure()
  plt.plot(powers, [madiff] * len(powers), linestyle='dashed', color='green', \
           label='Avg')
  plt.plot(powers, [0] * len(powers), linestyle='dashed', color='red',
           label='Max')
  plt.plot(powers, gmdiff, color='blue', label='Gem', marker='o',
           linestyle='dashed')
  plt.xlabel('Gem power')
  plt.ylabel('Matrix difference')
  plt.xlim([0, max(powers) + 1])
  plt.legend()
  plt.grid(linestyle='--', linewidth=0.5)
  plt.show()

def test_gem_max_avg_speeds(sparsity=0.5, gem_power=5.):
  # Define a test matrix.
  X = np.random.rand(1, 1024, 1024, 3)

  # Randomly set some elements to zero.
  indices = np.random.choice(X.size,
                             replace=False,
                             size=int(X.size * sparsity))
  X[np.unravel_index(indices, X.shape)] = 0

  # Convert X to tensor.
  X = tf.constant(X, dtype=tf.float32)

  # For different psize, time the calculation time.
  psizes = list(range(2, 16))
  gem_times = []
  max_times = []
  avg_times = []
  for psize in psizes:
    max_pool = tf.keras.layers.MaxPooling2D(pool_size=(psize, psize),
                                            strides=(1, 1), padding='valid')
    avg_pool = tf.keras.layers.AveragePooling2D(pool_size=(psize, psize),
                                                strides=(1, 1),
                                                padding='valid')
    gem_pool = pooling.GeMPooling2D(power=gem_power, pool_size=(psize, psize),
                                    strides=(1, 1), padding='valid')
    # Timing.
    n = 5
    max_start_time = time.time()
    for _ in range(n):
      max_pool(X)
    max_stop_time = time.time()
    max_times.append((max_stop_time - max_start_time)/n)

    gem_start_time = time.time()
    for _ in range(n):
      gem_pool(X)
    gem_stop_time = time.time()
    gem_times.append((gem_stop_time - gem_start_time)/n)

    avg_start_time = time.time()
    for _ in range(n):
      avg_pool(X)
    avg_stop_time = time.time()
    avg_times.append((avg_stop_time - avg_start_time)/n)


  # Visualization.
  plt.figure()
  plt.plot(psizes, avg_times, linestyle='dashed', marker='o',
           color='green', label='Avg')
  plt.plot(psizes, max_times, linestyle='dashed', marker='o',
           color='red', label='Max')
  plt.plot(psizes, gem_times, color='blue', label='Gem', marker='o',
           linestyle='dashed')
  plt.xlabel('Pooling size')
  plt.ylabel('Time [s]')
  plt.legend()
  plt.grid(linestyle = '--', linewidth = 0.5)
  plt.show()



if __name__=='__main__':
  np.random.seed(0)
  pool_test_2d(sparsity=0.5)
  os.environ['CUDA_VISIBLE_DEVICES'] = '0'
  #test_gem_max_avg_speeds(gem_power=5.)
  

